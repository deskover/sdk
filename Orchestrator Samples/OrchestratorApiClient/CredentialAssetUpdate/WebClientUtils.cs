﻿using System;
using System.Configuration;
using System.Net;

namespace CredentialAssetUpdate
{
    internal static class WebClientUtils
    {
        internal static void SetAuth(this WebClient client)
        {
            var username = ConfigurationManager.AppSettings["username"];
            if (!string.IsNullOrEmpty(username))
            {
                var pass = ConfigurationManager.AppSettings["password"];
                var domain = ConfigurationManager.AppSettings["domain"];
                CredentialCache cc = new CredentialCache();
                cc.Add(
                    new Uri(ConfigurationManager.AppSettings["rootUrl"]),
                    "NTLM",
                    new NetworkCredential(username, pass, domain));
                client.Credentials = cc;
            }
        }
    }
}
