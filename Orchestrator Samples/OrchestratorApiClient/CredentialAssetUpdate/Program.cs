﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;

namespace CredentialAssetUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            long assetId = -1;
            string assetName = null, assetUser = null, assetPass = null;

            if (!ParseArguments(args, ref assetId, ref assetName, ref assetUser, ref assetPass))
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                string name = Path.GetFileName(codeBase);
                Console.WriteLine($"\r\nUsage: {name} assetId password\r\n\r\nE.g.: {name} 7 myPass123");
                Environment.Exit(-1);
            }

            UpdateCredentialAsset(assetId, assetName, assetUser, assetPass);
        }

        #region Change Password in global credential

        private static bool ParseArguments(string[] args, ref long assetId, ref string assetName, ref string assetUser, ref string assetPass)
        {
            string errorMsg = null;
            if (args == null || args.Length != 4)
            {
                errorMsg = "Argument error: Incorrect number of arguments";
            }
            else
            {
                string assetIdStr = args[0];
                assetName = args[1];
                assetUser = args[2];
                assetPass = args[3];

                if (string.IsNullOrEmpty(assetIdStr) || !long.TryParse(assetIdStr, out assetId))
                {
                    errorMsg = "Argument error: assetId parameter is incorrect";
                }
                else if (string.IsNullOrEmpty(assetName))
                {
                    errorMsg = "Argument error: assetName parameter is emtpy";
                }
                else if (string.IsNullOrEmpty(assetUser))
                {
                    errorMsg = "Argument error: assetUser parameter is emtpy";
                }
                else if (string.IsNullOrEmpty(assetPass))
                {
                    errorMsg = "Argument error: assetPass parameter is emtpy";
                }
            }

            if (errorMsg != null)
            {
                Console.WriteLine(errorMsg);
                return false;
            }
            else
                return true;
        }

        private static void UpdateCredentialAsset(long assetId, string assetName, string assetUser, string assetPass)
        {
            try
            {
                var rootUrl = ConfigurationManager.AppSettings["rootUrl"];
                var relativeUrl = ConfigurationManager.AppSettings["relativeUrl"];
                var url = $"{rootUrl}{relativeUrl}({assetId})";
                var data = GetAssetUpdateContentString(url, assetId, assetName, assetUser, assetPass);

                using (var client = new WebClient())
                {
                    client.SetAuth();
                    client.Headers.Add(HttpRequestHeader.ContentType, "application/json; charset=utf-8");

                    client.UploadData(url, "PUT", Encoding.UTF8.GetBytes(data));
                }
            }
            catch (WebException wex)
            {
                Console.WriteLine(wex.ToString());
                using (var reader = new StreamReader(wex.Response.GetResponseStream(), Encoding.UTF8))
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
            }
        }

        private static string GetAssetUpdateContentString(string url, long assetId, string assetName, string assetUser, string assetPass)
        {
            if (string.IsNullOrEmpty(assetUser))
            {
                Console.WriteLine("Username: ");
                assetUser = Console.ReadLine();
            }
            var content = $"{{ 'Name':'{assetName}','CredentialUsername':'{assetUser}','CredentialPassword':'{assetPass}','DistributionType':'AllProcesses','CanBeDeleted':true,'ValueType':'Credential','HasValuePerRobot':false,'Value':'username: {assetUser}','StringValue':null,'BoolValue':false,'IntValue':-1,'DoUpdateCredentials':true,'KeyValueList':[],'Id':{assetId},'Href':'{url}','RobotValues':[]}}";
            return content;
        }
        #endregion
    }
}
