﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;

namespace RobotPassUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            string robotKey = null, robotPass = null;

            if (!ParseArguments(args, ref robotKey, ref robotPass))
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                string name = Path.GetFileName(codeBase);
                Console.WriteLine($"\r\nUsage: {name} robot-key password\r\n\r\nE.g.: {name} 01234567-0123-0123-0123-012345678901 myPass123");
                Environment.Exit(-1);
            }

            ChangeRobotPass(robotKey, robotPass);
        }

        #region Change Robot Windows Password

        private static bool ParseArguments(string[] args, ref string robotKey, ref string robotPass)
        {
            string errorMsg = null;
            if (args == null || args.Length != 2)
            {
                errorMsg = "Argument error: Incorrect number of arguments";
            }
            else
            {
                robotKey = args[0];
                robotPass = args[1];
                Guid robotKeyGuid;
                if (string.IsNullOrEmpty(robotKey) || !Guid.TryParse(robotKey, out robotKeyGuid))
                {
                    errorMsg = "Argument error: RobotKey parameter is incorrect";
                }
                if (string.IsNullOrEmpty(robotPass))
                {
                    errorMsg = "Argument error: RobotPas parameter is emtpy";
                }
            }

            if (errorMsg != null)
            {
                Console.WriteLine(errorMsg);
                return false;
            }
            else
                return true;
        }

        private static void ChangeRobotPass(string robotKey, string robotPass)
        {
            try
            {
                var robotsUrl = string.Concat(ConfigurationManager.AppSettings["rootUrl"], ConfigurationManager.AppSettings["relativeUrl"]);

                using (var client = new WebClient())
                {
                    client.SetAuth();

                    string robots = Encoding.UTF8.GetString(client.DownloadData(robotsUrl));
                    var robotsDyn = ((dynamic)JsonConvert.DeserializeObject(robots)).value;
                    foreach (var robot in robotsDyn)
                    {
                        if (robot.Key.ToString().ToLower() == robotKey.ToLower())
                        {
                            robot.Password = robotPass;
                            var updatedData = JsonConvert.SerializeObject(robot);
                            var singleRobotUrl = $"{robotsUrl}({robot.Id})";

                            client.Headers.Add(HttpRequestHeader.ContentType, "application/json; charset=utf-8");
                            client.UploadData(singleRobotUrl, "PUT", Encoding.UTF8.GetBytes(updatedData));

                            Console.WriteLine("Password changed successfully");
                            return;
                        }
                    }

                    Console.WriteLine("No robot with this key was found");
                    Environment.Exit(-1);
                }
            }
            catch (WebException wex)
            {
                Console.WriteLine(wex.ToString());
                using (var reader = new System.IO.StreamReader(wex.Response.GetResponseStream(), Encoding.UTF8))
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
                Environment.Exit(-1);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                Environment.Exit(-1);
            }
        }
        #endregion
    }
}
